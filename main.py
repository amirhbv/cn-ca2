import os
import subprocess

import matplotlib.pyplot as plt

MAX_TIME = 1000
STEPS = 10
TR_DIR = 'tr'
NAM_DIR = 'nam'
SRC_DIR = 'src'
DATA_DIR = 'data'


def getDropRates(tcp_type):
    tracefile = f'{TR_DIR}/{tcp_type}.tr'
    totalPacketSent1, totalPacketSent2 = 0, 0
    totalPacketReceived1, totalPacketReceived2 = 0, 0
    dropRates1 = [0]
    dropRates2 = [0]
    currentTime = 0

    with open(tracefile) as f:
        for line in f:
            sec = line.split()
            event = sec[0]
            time = float(sec[1])
            from_node = sec[2]
            to_node = sec[3]
            pkttype = sec[4]
            pktsize = int(sec[5])
            flow_id = sec[7]
            src_addr = sec[8]
            dst_addr = sec[9]
            seq_num = sec[10]
            if flow_id == '1':
                if event == '-' and src_addr == '0.0':
                    totalPacketSent1 += 1
                if event == 'r' and dst_addr == '4.0' and pkttype == 'ack':
                    totalPacketReceived1 += 1
            elif flow_id == '2':
                if event == '-' and src_addr == '1.0':
                    totalPacketSent2 += 1
                if event == 'r' and dst_addr == '5.0' and pkttype == 'ack':
                    totalPacketReceived2 += 1

            if time - currentTime > 1:
                if totalPacketSent1 == 0:
                    dropRates1.append(0)
                else:
                    dropRates1.append(
                        float(totalPacketSent1 - totalPacketReceived1) / totalPacketSent1 * 100)

                if totalPacketSent2 == 0:
                    dropRates2.append(0)
                else:
                    dropRates2.append(
                        float(totalPacketSent2 - totalPacketReceived2) / totalPacketSent2 * 100)

                currentTime = time
                totalPacketSent = 0
                totalPacketReceived = 0
    return dropRates1, dropRates2


def run(tcp_type):
    agent1_cwnds = [0 for _ in range(MAX_TIME)]
    agent2_cwnds = [0 for _ in range(MAX_TIME)]

    agent1_rtts = [0 for _ in range(MAX_TIME)]
    agent2_rtts = [0 for _ in range(MAX_TIME)]

    agent1_goodputs = [0 for _ in range(MAX_TIME)]
    agent2_goodputs = [0 for _ in range(MAX_TIME)]

    for i in range(STEPS):
        print(f'running {tcp_type} - step {i}')

        subprocess.call([
            'ns', f'{SRC_DIR}/{tcp_type}.tcl'
        ])

        with open(f'{DATA_DIR}/{tcp_type}.data') as fin:
            for line in fin:
                time, cwnd1, goodput1, rtt1, cwnd2, goodput2, rtt2 = line.split(
                    ' ')

                time = int(time)

                agent1_cwnds[time] += float(cwnd1)
                agent2_cwnds[time] += float(cwnd2)

                agent1_rtts[time] += float(rtt1)
                agent2_rtts[time] += float(rtt2)

                agent1_goodputs[time] += float(goodput1)
                agent2_goodputs[time] += float(goodput2)

    agent1_cwnds = [x / STEPS for x in agent1_cwnds]
    agent2_cwnds = [x / STEPS for x in agent2_cwnds]
    agent1_rtts = [x / STEPS for x in agent1_rtts]
    agent2_rtts = [x / STEPS for x in agent2_rtts]
    agent1_goodputs = [x / STEPS for x in agent1_goodputs]
    agent2_goodputs = [x / STEPS for x in agent2_goodputs]

    return agent1_cwnds, agent2_cwnds, \
        agent1_rtts, agent2_rtts, \
        agent1_goodputs, agent2_goodputs


try:
    os.mkdir(DATA_DIR)
except:
    pass

try:
    os.mkdir(NAM_DIR)
except:
    pass

try:
    os.mkdir(TR_DIR)
except:
    pass

x = [x for x in range(MAX_TIME)]

newreno_agent1_cwnds, newreno_agent2_cwnds, \
    newreno_agent1_rtts, newreno_agent2_rtts, \
    newreno_agent1_goodputs, newreno_agent2_goodputs = run('newreno')
newreno_agent1_drops, newreno_agent2_drops = getDropRates('newreno')

tahoe_agent1_cwnds, tahoe_agent2_cwnds, \
    tahoe_agent1_rtts, tahoe_agent2_rtts, \
    tahoe_agent1_goodputs, tahoe_agent2_goodputs = run('tahoe')
tahoe_agent1_drops, tahoe_agent2_drops = getDropRates('tahoe')

vegas_agent1_cwnds, vegas_agent2_cwnds, \
    vegas_agent1_rtts, vegas_agent2_rtts, \
    vegas_agent1_goodputs, vegas_agent2_goodputs = run('vegas')
vegas_agent1_drops, vegas_agent2_drops = getDropRates('vegas')

fig, ax = plt.subplots()
fig.suptitle('CWND')
fig.set_figheight(20)
fig.set_figwidth(40)
ax.plot(x, newreno_agent1_cwnds, label="newreno_agent1_cwnds")
ax.plot(x, newreno_agent2_cwnds, label="newreno_agent2_cwnds")
ax.plot(x, tahoe_agent1_cwnds, label="tahoe_agent1_cwnds")
ax.plot(x, tahoe_agent2_cwnds, label="tahoe_agent2_cwnds")
ax.plot(x, vegas_agent1_cwnds, label="vegas_agent1_cwnds")
ax.plot(x, vegas_agent2_cwnds, label="vegas_agent2_cwnds")
ax.set_xlabel('time')
ax.set_ylabel('cwnd')
ax.legend()

fig, ax = plt.subplots()
fig.suptitle('RTT')
fig.set_figheight(20)
fig.set_figwidth(40)
ax.plot(x, newreno_agent1_rtts, label="newreno_agent1_rtts")
ax.plot(x, newreno_agent2_rtts, label="newreno_agent2_rtts")
ax.plot(x, tahoe_agent1_rtts, label="tahoe_agent1_rtts")
ax.plot(x, tahoe_agent2_rtts, label="tahoe_agent2_rtts")
ax.plot(x, vegas_agent1_rtts, label="vegas_agent1_rtts")
ax.plot(x, vegas_agent2_rtts, label="vegas_agent2_rtts")
ax.set_xlabel('time')
ax.set_ylabel('rtt')
ax.legend()

fig, ax = plt.subplots()
fig.suptitle('GoodPut')
fig.set_figheight(20)
fig.set_figwidth(40)
ax.plot(x, newreno_agent1_goodputs, label="newreno_agent1_goodputs")
ax.plot(x, newreno_agent2_goodputs, label="newreno_agent2_goodputs")
ax.plot(x, tahoe_agent1_goodputs, label="tahoe_agent1_goodputs")
ax.plot(x, tahoe_agent2_goodputs, label="tahoe_agent2_goodputs")
ax.plot(x, vegas_agent1_goodputs, label="vegas_agent1_goodputs")
ax.plot(x, vegas_agent2_goodputs, label="vegas_agent2_goodputs")
ax.set_xlabel('time')
ax.set_ylabel('goodput')
ax.legend()


fig, ax = plt.subplots()
fig.suptitle('DropRate')
fig.set_figheight(20)
fig.set_figwidth(40)
ax.plot(x, newreno_agent1_drops, label="newreno_agent1_drops")
ax.plot(x, newreno_agent2_drops, label="newreno_agent2_drops")
ax.plot(x, tahoe_agent1_drops, label="tahoe_agent1_drops")
ax.plot(x, tahoe_agent2_drops, label="tahoe_agent2_drops")
ax.plot(x, vegas_agent1_drops, label="vegas_agent1_drops")
ax.plot(x, vegas_agent2_drops, label="vegas_agent2_drops")
ax.set_xlabel('time')
ax.set_ylabel('DropRate')
ax.legend()

plt.show()
