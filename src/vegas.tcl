# Define "TraceApp" as a child class of "Application"
Class TraceApp -superclass Application

# Define (override) "init" method to create "TraceApp" object
TraceApp instproc init {args} {
    $self set bytes_ 0
    eval $self next $args
}

# Define (override) "recv" method for "TraceApp" object
TraceApp instproc recv {byte} {
    $self instvar bytes_
    set bytes_ [expr $bytes_ + $byte]
    return $bytes_
}

# Create a simulator object
set ns [new Simulator]

# Define different colors for data flows (for NAM)
$ns color 1 Blue
$ns color 2 Red

# Open the nam file and the variable-trace file
set tracefile [open "tr/vegas.tr" w]
$ns trace-all $tracefile

set namfile [open "nam/vegas.nam" w]
$ns namtrace-all $namfile

# Define a 'finish' procedure
proc finish {} {
    global ns namfile tracefile
    $ns flush-trace
    close $namfile
    close $tracefile
    # exec nam main.nam &
    exit 0
}

proc plot {outFile tcpSource1 tcpSource2 tcpSink1 tcpSink2} {
    global ns

    set timeStep 1.0
    set now [$ns now]

    set cwnd1 [$tcpSource1 set cwnd_]
    set cwnd2 [$tcpSource2 set cwnd_]

    set rtt1 [$tcpSource1 set rtt_]
    set rtt2 [$tcpSource2 set rtt_]

    set nbytes1 [$tcpSink1 set bytes_]
    $tcpSink1 set bytes_ 0
    set goodput1 [expr ($nbytes1 * 8.0) / $timeStep]

    set nbytes2 [$tcpSink2 set bytes_]
    $tcpSink2 set bytes_ 0
    set goodput2 [expr ($nbytes2 * 8.0) / $timeStep]

    puts $outFile "$now $cwnd1 $goodput1 $rtt1 $cwnd2 $goodput2 $rtt2"

    $ns at [expr $now + $timeStep] "plot $outFile $tcpSource1 $tcpSource2 $tcpSink1 $tcpSink2"
}


# Create the network nodes
set n1 [$ns node]
set n2 [$ns node]
set r3 [$ns node]
set r4 [$ns node]
set n5 [$ns node]
set n6 [$ns node]

set rand_delay1 [new RandomVariable/Uniform];
$rand_delay1 set min_ 5ms
$rand_delay1 set max_ 25ms

set rand_delay2 [new RandomVariable/Uniform];
$rand_delay2 set min_ 5ms
$rand_delay2 set max_ 25ms

# Create links
$ns duplex-link $n1 $r3 100Mb 5ms DropTail
$ns duplex-link $n2 $r3 100Mb rand_delay1 DropTail
$ns duplex-link $r3 $r4 100Kb 1ms DropTail
$ns duplex-link $r4 $n5 100Mb 5ms DropTail
$ns duplex-link $r4 $n6 100Mb rand_delay2 DropTail

# The queue limit is 10
$ns queue-limit $n1 $r3 10
$ns queue-limit $n2 $r3 10
$ns queue-limit $r3 $r4 10
$ns queue-limit $r4 $n5 10
$ns queue-limit $r4 $n6 10


set tcp1 [new Agent/TCP/Vegas]
$tcp1 set fid_ 1
$tcp1 set ttl_ 64
set sink1 [new Agent/TCPSink]
$ns attach-agent $n1 $tcp1
$ns attach-agent $n5 $sink1
$ns connect $tcp1 $sink1

set tcp2 [new Agent/TCP/Vegas]
$tcp2 set fid_ 2
$tcp1 set ttl_ 64
set sink2 [new Agent/TCPSink]
$ns attach-agent $n2 $tcp2
$ns attach-agent $n6 $sink2
$ns connect $tcp2 $sink2

# Setup a CBR over TCP connection
set cbr1 [new Application/Traffic/CBR]
$cbr1 attach-agent $tcp1
$cbr1 set type_ CBR
$cbr1 set packet_size_ 1000

set cbr2 [new Application/Traffic/CBR]
$cbr2 attach-agent $tcp2
$cbr2 set type_ CBR
$cbr2 set packet_size_ 1000

# Setup traceapp for goodput
set traceapp1 [new TraceApp]
$traceapp1 attach-agent $sink1

set traceapp2 [new TraceApp]
$traceapp2 attach-agent $sink2

$ns at 0.0 "$traceapp1 start"
$ns at 0.0 "$traceapp2 start"

set outFile [open "data/vegas.data"  w]
$ns at 0.0 "plot $outFile $tcp1 $tcp2 $sink1 $sink2"

$ns at .1 "$cbr1 start"
$ns at .1 "$cbr2 start"

$ns at 1000 "$cbr1 stop"
$ns at 1000 "$cbr2 stop"

$ns at 1000 "finish"

# Run simulation !!!!
$ns run
